from rest_framework import generics
from .models import Customer, MailingList, Message
from .serializers import CustomerSerializer, MailingListSerializer, MessageSerializer, StatisticsSerializer
from rest_framework.response import Response
from rest_framework import status
from .tasks import start_mailing

from datetime import datetime


from rest_framework import viewsets


class CustomerList(generics.ListCreateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer


class CustomerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer


class MailingListList(generics.ListCreateAPIView):
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer

    def perform_create(self, serializer):
        mailing_list = serializer.save()
        if mailing_list:
            print(self.request.data)
            start_time = datetime.strptime(self.request.data['start_datetime'][:19], '%Y-%m-%dT%H:%M:%S')
            finish_time = datetime.strptime(self.request.data['finish_datetime'][:19], '%Y-%m-%dT%H:%M:%S')

            if start_time < datetime.now() < finish_time:
                start_mailing.apply_async(mailing_list.id, expires=finish_time)
            elif datetime.now() < start_time:
                start_mailing.apply_async(mailing_list.id, eta=start_time, expires=finish_time)


class MailingListDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer


class MessagesList(generics.ListAPIView):
    serializer_class = MessageSerializer

    def get_queryset(self):
        return Message.objects.filter(mailing_list__id=self.kwargs['mailing_list_id'])


class Statistics(generics.ListAPIView):
    serializer_class = StatisticsSerializer
    queryset = MailingList.objects.all()
