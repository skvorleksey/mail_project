from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator
import pytz


class MailingList(models.Model):
    start_datetime = models.DateTimeField(default=timezone.now)
    message_text = models.TextField(default='Text message')
    operator_code = models.CharField(max_length=3, validators=[RegexValidator(r'\d{3}')], default='xxx')
    tag = models.CharField(max_length=100, null=True)
    finish_datetime = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.message_text[:50]} ...'


class Customer(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(
        max_length=11,
        validators=[RegexValidator(r'7\d{10}')])
    operator_code = models.CharField(max_length=3, validators=[RegexValidator(r'\d{3}')])
    tag = models.CharField(max_length=100)

    timezone = models.CharField(
        max_length=32,
        choices=TIMEZONES,
        default='UTC')

    def __str__(self):
        return self.phone_number


class Message(models.Model):
    sent_datetime = models.DateTimeField(default=timezone.now)
    sending_status = models.IntegerField()
    mailing_list = models.ForeignKey(MailingList, related_name='messages', on_delete=models.CASCADE)
    client = models.ForeignKey(Customer, on_delete=models.CASCADE)

    def __str__(self):
        return f'Message in mailing list {self.mailing_list}'
