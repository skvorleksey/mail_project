from rest_framework.test import APITestCase
from mail_app.models import MailingList, Customer
from datetime import datetime
from django.urls import reverse
from rest_framework import status


class TestMailingListList(APITestCase):
    def setUp(self):
        MailingList.objects.create(message_text='Message',
                                   operator_code='999',
                                   tag='Some tag',
                                   finish_datetime=datetime.now())
        MailingList.objects.create(message_text='Message',
                                   operator_code='999',
                                   tag='Some tag',
                                   finish_datetime=datetime.now())

    def test_gets_mailing_lists(self):
        response = self.client.get(reverse('mail_app:mailing_lists'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_should_create_mailing_list(self):
        amount = MailingList.objects.all().count()
        data = {"start_datetime": "2022-08-29T07:58:31.991Z",
                "message_text": "string",
                "operator_code": "999",
                "tag": "tag",
                "finish_datetime": "2022-08-29T07:58:31.991Z"
                }
        response = self.client.post(reverse('mail_app:mailing_lists'), data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(amount+1, MailingList.objects.all().count())


class TestCustomerList(APITestCase):
    def setUp(self):
        Customer.objects.create(phone_number='71234567899',
                                operator_code='123',
                                tag='Some tag',
                                timezone='UTC')
        Customer.objects.create(phone_number='71234567899',
                                operator_code='123',
                                tag='Some tag',
                                timezone='UTC')

    def test_gets_customers(self):
        response = self.client.get(reverse('mail_app:customer_list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_should_create_mailing_list(self):
        amount = Customer.objects.all().count()
        data = {"phone_number": "71234567899",
                "operator_code": "123",
                "tag": "Some tag",
                "timezone": "UTC"
                }
        response = self.client.post(reverse('mail_app:customer_list'), data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(amount+1, Customer.objects.all().count())
