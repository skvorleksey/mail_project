from django.urls import path
from . import views

app_name = 'mail_app'

urlpatterns = [
    path('customers/', views.CustomerList.as_view(), name='customer_list'),
    path('customers/<int:pk>/', views.CustomerDetail.as_view(), name='customer_detail'),
    path('mailing_lists', views.MailingListList.as_view(), name='mailing_lists'),
    path('mailing_lists/<int:pk>', views.MailingListDetail.as_view(), name='mailing_list_detail'),
    path('statistics/<int:mailing_list_id>/', views.MessagesList.as_view(), name='messages_list'),
    path('statistics/', views.Statistics.as_view(), name='all_mailing_lists_messages'),
]
