from django.contrib import admin
from .models import MailingList, Customer, Message


class MailingListAdmin(admin.ModelAdmin):
    list_display = ('id', 'start_datetime', 'message_text', 'operator_code', 'tag', 'finish_datetime')


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('id', 'phone_number', 'operator_code', 'tag', 'timezone')


class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'sent_datetime', 'sending_status', 'mailing_list', 'client')


admin.site.register(MailingList, MailingListAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Message, MessageAdmin)
