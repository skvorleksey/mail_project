from celery import shared_task
import requests
from mail_project.settings import FBRQ_JWT_TOKEN
from .models import MailingList, Message, Customer


url = 'https://probe.fbrq.cloud/v1/send/'
headers = {'Authorization': f'Bearer {FBRQ_JWT_TOKEN}',
           'Content-Type': 'application/json',
           'accept': 'application/json',
           }


@shared_task(name='start_mailing')
def start_mailing(mailing_list_id):
    mailing_list = MailingList.objects.get(id=mailing_list_id)
    customers = Customer.objects.filter(operator_code=mailing_list.operator_code,
                                        tag=mailing_list.tag)

    for customer in customers:
        message = Message.objects.create(
            mailing_list=mailing_list,
            client=customer,
            sending_status=-1
        )

        data = {
            "id": message.id,
            "phone": int(customer.phone_number),
            "text": mailing_list.message_text,
            }

        response = requests.request('POST', url+str(message.id), headers=headers, json=data)
        if response.ok:
            message.sending_status = response.json()['code']

        message.save()
