**Зависимости находятся в файле requirements.txt**<br>
pip install -r requirements.txt

**Установить redis**<br>
https://redis.io/docs/getting-started/installation/install-redis-on-linux/#install-on-ubuntu<br>
curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

sudo apt-get update
sudo apt-get install redis

**Настройка и запуск проекта**<br>
https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-18-04-ru

**Запуск celery**<br>
celery -A mail_project worker -l INFO

Или запуск celery через systemd
https://docs.celeryq.dev/en/stable/userguide/daemonizing.html

<hr>

**Административная панель доступна по адресу /admin/**

**Swagger документация доступна по адресу /docs/**
